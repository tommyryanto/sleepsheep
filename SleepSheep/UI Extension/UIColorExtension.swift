//
//  UIColorExtension.swift
//  SleepSheep
//
//  Created by Tommy Ryanto on 21/05/19.
//  Copyright © 2019 Tommy Ryanto. All rights reserved.
//

import UIKit

extension UIColor{
    convenience init (red: CGFloat, green: CGFloat, blue: CGFloat){
        self.init(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
}
