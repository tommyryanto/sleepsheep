//
//  LifeCycle.swift
//  SleepSheep
//
//  Created by Tommy Ryanto on 21/05/19.
//  Copyright © 2019 Tommy Ryanto. All rights reserved.
//

import UIKit

extension ViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //view.backgroundColor = .orange
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        createGradient()
        
        configureAudio()
        
        initUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        configureTimer()
    }
}
