//
//  VC + UI.swift
//  SleepSheep
//
//  Created by Tommy Ryanto on 21/05/19.
//  Copyright © 2019 Tommy Ryanto. All rights reserved.
//

import UIKit

extension ViewController{
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    func clipOldBall(){
        if timeLaunched > 4{
            let oldBall: UIButton = {
                let view = UIButton()
                //view.backgroundColor = .
                view.frame = CGRect(x: self.ball.frame.origin.x, y: self.ball.frame.origin.y, width: 100, height: 100)
                view.layer.cornerRadius = view.frame.size.width / 2
                view.setImage(UIImage(named: "sheep"), for: .normal)
                return view
            }()
            self.view.addSubview(oldBall)
            self.addAction(oldBall)
        }
    }
    
    func animateBall(){
        UIView.animate(withDuration: 5, delay: 0, options: [], animations: {
            var CGFloatRandomForWidth = CGFloat()
            var CGFloatRandomForHeight = CGFloat()
            print("yBall: \(self.ball.frame.origin.y)")
            print("xBall: \(self.ball.frame.origin.x)")
            print("heightView: \(self.view.frame.height)")
            print("widthView: \(self.view.frame.width)")
            if abs(self.ball.frame.origin.y) + 100 < self.view.frame.height - 10 && abs(self.ball.frame.origin.x) + 100 < self.view.frame.width - 10 {
                //ball not in the border of x and y
                CGFloatRandomForWidth = .random(in: 10...self.view.frame.width - self.ball.frame.maxX)
                CGFloatRandomForHeight = .random(in: 10...self.view.frame.height - self.ball.frame.maxY)
                let randomAction = Int.random(in: 0...1)
                if self.ball.frame.origin.y < self.view.frame.height / 4{
                    print("option 1.1")
                    self.ball.frame.origin.x += CGFloatRandomForWidth
                    self.ball.frame.origin.y += CGFloatRandomForHeight
                }else{
                    switch randomAction{
                    case 0:
                        print("option 1.2.1")
                        self.ball.frame.origin.x += CGFloatRandomForWidth
                        self.ball.frame.origin.y += CGFloatRandomForHeight
                    case 1:
                        print("option 1.2.2")
                        CGFloatRandomForHeight = .random(in: 10...self.ball.frame.maxY)
                        self.ball.frame.origin.x += CGFloatRandomForWidth
                        self.ball.frame.origin.y -= CGFloatRandomForHeight
                    default:
                        self.doNothing()
                    }
                }
            }else if abs(self.ball.frame.origin.y) + 100 < self.view.frame.height - 10 && abs(self.ball.frame.origin.x) + 100 > self.view.frame.width - 10 {
                //ball in the border of x but not in the border of y
                print("option 2")
                CGFloatRandomForWidth = .random(in: 10...self.view.frame.width)
                CGFloatRandomForHeight = .random(in: 10...self.view.frame.height - self.ball.frame.maxY)
                
                self.ball.frame.origin.x -= CGFloatRandomForWidth
                self.ball.frame.origin.y += CGFloatRandomForHeight
            }else if abs(self.ball.frame.origin.y) + 100 > self.view.frame.height - 10 && abs(self.ball.frame.origin.y) + 100 < self.view.frame.width - 10{
                //ball not in the border of x but in the border of y
                print("option 3")
                CGFloatRandomForWidth = .random(in: 10...self.view.frame.width - self.ball.frame.origin.x)
                CGFloatRandomForHeight = .random(in: 10...self.view.frame.height)
                self.ball.frame.origin.x += CGFloatRandomForWidth
                self.ball.frame.origin.y -= CGFloatRandomForHeight
            }
            else{
                //ball in the border of x and y
                CGFloatRandomForWidth = .random(in: 10...self.ball.frame.maxX)
                CGFloatRandomForHeight = .random(in: 10...self.view.frame.height)
                if self.ball.frame.origin.x > self.view.frame.midX{
                    print("option 4.1")
                    self.ball.frame.origin.x -= CGFloatRandomForWidth
                }else{
                    print("option 4.2")
                    self.ball.frame.origin.x += CGFloatRandomForWidth
                }
                if self.ball.frame.origin.y > self.view.frame.midY{
                    print("option 4.3")
                    
                    self.ball.frame.origin.y -= CGFloatRandomForHeight
                }else{
                    print("option 4.4")
                    self.ball.frame.origin.y += CGFloatRandomForHeight
                }
            }
        }) { (true) in
            //complete
            if self.timeLaunched <= 600{
                self.animateUI()
            }else{
                self.audioPlayer.stop()
                self.present(self.alert, animated: true, completion: {
                    self.doNothing()
                })
                self.alertTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateAlertTime), userInfo: nil, repeats: true)
                self.alertTimer.fire()
            }
        }
    }

    func animateUI(){
        clipOldBall()
        animateBall()
    }
    
    func createGradient() {
        let colorTop =  UIColor(red: 255.0/255.0, green: 192/255.0, blue: 93/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 242/255.0, green: 102/255.0, blue: 9/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.view.bounds
        
        self.view.layer.insertSublayer(gradientLayer, at:0)
    }
    
    func addAction(_ sender: UIButton){
        sender.addTarget(self, action: #selector(ballAction(_:)), for: .touchUpInside)
    }
    
    @objc func ballAction(_ sender: UIButton){
        print("sheep clicked")
        let balls: UIButton = {
            let view = UIButton()
            //view.backgroundColor = .
            view.frame = CGRect(x: .random(in: 0...self.view.frame.width), y: .random(in: 0...self.view.frame.height), width: 100, height: 100)
            view.layer.cornerRadius = view.frame.size.width / 2
            view.setImage(UIImage(named: "sheep"), for: .normal)
            return view
        }()
        self.view.addSubview(balls)
        animateNewBall(sender: balls)
    }
    
    func animateNewBall(sender: UIButton){
        //var count = 0
        let randomX = CGFloat.random(in: 0...self.view.frame.width)
        let randomY = CGFloat.random(in: 0...self.view.frame.height)
        let difX = randomX-sender.frame.maxX
        let difY = abs(randomY-sender.frame.maxY)
        UIView.animate(withDuration: 5, animations: {
            sender.frame.origin.x += difX
            sender.frame.origin.y += difY
        }) { (true) in
            //count+=1
        }
    }
    
    func initUI(){
        view.addSubview(ball)
        animateUI()
        //addAction()
    }
}
