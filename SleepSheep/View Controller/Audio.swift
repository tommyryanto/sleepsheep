//
//  Audio.swift
//  SleepSheep
//
//  Created by Tommy Ryanto on 21/05/19.
//  Copyright © 2019 Tommy Ryanto. All rights reserved.
//

import AVFoundation

extension ViewController{
    func configureAudio(){
        do{
            assortedMusic = URL(fileURLWithPath: Bundle.main.path(forResource: "music", ofType: "mp3")!)
            audioPlayer = try AVAudioPlayer(contentsOf: assortedMusic!)
            
            audioPlayer.prepareToPlay()
            
            audioPlayer.numberOfLoops = -1
            audioPlayer.volume = 5
            audioPlayer.play()
        }catch{
            print("audio player can't be played.")
        }
    }
}
