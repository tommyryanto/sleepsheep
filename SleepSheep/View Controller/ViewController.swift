//
//  ViewController.swift
//  SleepSheep
//
//  Created by Tommy Ryanto on 16/05/19.
//  Copyright © 2019 Tommy Ryanto. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    var timeLaunched: Float = 0
    var alertLaunched: Float = 0
    var audioPlayer = AVAudioPlayer()
    var alertTimer = Timer()
    var assortedMusic: URL?
    
    let ball: UIButton = {
        let view = UIButton()
        //view.backgroundColor = .
        view.frame = CGRect(x: 30, y: 50, width: 100, height: 100)
        view.layer.cornerRadius = view.frame.size.width / 2
        view.setImage(UIImage(named: "sheep"), for: .normal)
        
        return view
    }()
    
    lazy var alert: UIAlertController = {
        let controller = UIAlertController(title: "Are you sleeping?", message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "No", style: .default, handler: { (action) in
            self.timeLaunched = 0
            self.alertLaunched = 0
            self.alertTimer.invalidate()
            self.animateUI()
            self.audioPlayer.play()
            self.configureTimer()
        })
        controller.addAction(action)
        return controller
    }()
    
    func doNothing(){}
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        timeLaunched = 0
    }
   
}
