//
//  Timer.swift
//  SleepSheep
//
//  Created by Tommy Ryanto on 21/05/19.
//  Copyright © 2019 Tommy Ryanto. All rights reserved.
//

import UIKit

extension ViewController{
    func configureTimer(){
        let timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        timer.fire()
    }
    
    @objc func updateAlertTime(){
        if alertLaunched < 300{
            alertLaunched += 1
            print("alert launched : \(alertLaunched)")
        }else{
            exit(0)
        }
    }
    
    @objc func updateTime(){
        if timeLaunched < 600{
            timeLaunched += 1
            print(timeLaunched)
        }else{
            doNothing()
        }
    }
}

